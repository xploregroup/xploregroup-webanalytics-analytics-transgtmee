var assert = require('assert');
var sinon = require('sinon');

var MockBrowser = require('mock-browser').mocks.MockBrowser;
var mock = new MockBrowser();
global.window = mock.getWindow();
global.window.document = mock.getDocument();
global.window.dataLayer = [];

var atGTM = require('../analyticsTransGTMEE');
var transInstance = atGTM();

describe('analyticsTransGTMEE', function () {
  it('should have analyticstracker', function () {
    assert.equal(typeof transInstance.tracker, 'object');
  });
});
