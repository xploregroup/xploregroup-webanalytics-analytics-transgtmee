/*!
* AnalyticsTransGTMEE.js Library v1.0.0
*
* Copyright 2017, Stefan Maris
* MIT Licensed (http://www.opensource.org/licenses/mit-license.php)
*
*
* Last update: December 5, 2017
*/
(function (root, factory) {

  'use strict';

  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['analyticstracker'], function (analyticstracker) {
			return (root.analyticsTransGTMEE = factory(analyticstracker, window));
		  });
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like enviroments that support module.exports,
    // like Node.
    module.exports = factory(require('analyticstracker'), window);
  } else {
    // Browser globals
    root.analyticsTransGTMEE = factory(root.analyticstracker, window);
  }

}(this, function (analyticstracker, w) {
// UMD Definition above, do not remove this line
  'use strict';

  var _instance;

  var analyticsTransGTMEE = function analyticsTransGTMEE() {
    if (!(this instanceof analyticsTransGTMEE)) {
      return new analyticsTransGTMEE();
    }

    this.tracker = analyticstracker();
    if ((typeof w != "undefined") && (w != null)) {
      w.dataLayer = w.dataLayer || [];
	}
	this._version = "1.0.0";
    this._subscribe();
  }

  analyticsTransGTMEE.prototype.getInstance = function() {
    if (!_instance) {
        _instance = analyticsTransGTMEE();
    }
    return _instance;
  }

  analyticsTransGTMEE.prototype._subscribe = function() {
    try {
      this.tracker.trackingSubscribe("GTMEE", function (event, data) {
    		console.log("GTMEE tracker: " + event);
      	console.log(data);
      	console.log("********");

    		var gtmData = {};

        function clone(a) {
        	return JSON.parse(JSON.stringify(a));
        }

        function isArray(obj) {
            return !!obj && obj.constructor === Array;
        }

    		switch (event) {
    			case "product-impression" :
    				if (data.hasOwnProperty("commerce")) {
    					// single element
    					var currencyCode = (data.commerce.hasOwnProperty("currency")) ?  data.commerce.currency : "EUR";
    					gtmData = {
    						'event' : 'product-impression',
    						'ecommerce' : {
    							'currencyCode' : currencyCode,
    							'impressions' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    						}
    					};
    				}
    				break;

          case "product-detail-impression" :
    				if (data.hasOwnProperty("commerce")) {
              var list = (data.commerce.hasOwnProperty("list")) ? data.commerce.list : "";
    					gtmData = {
    						'event' : 'product-impression',
    						'ecommerce' : {
                  'detail' : {
      							'actionField' : {'list' : list},
      							'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    						  }
                }
    					};
    				}
    				break;

    			case "product-click":
    				if (data.hasOwnProperty("commerce")) {
    					var list = (data.commerce.hasOwnProperty("list")) ? data.commerce.list : "";
    					gtmData = {
    						  'event' : 'product-click',
    						  'ecommerce' : {
    								'click' : {
    									'actionField' : {'list' : list},
    									'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    								}
    							}
    					};
    				}
    				break;

    			case "product-detail":
    				if (data.hasOwnProperty("commerce")) {
    					var list = (data.commerce.hasOwnProperty("list")) ? data.commerce.list : "";
    					gtmData = {
    						  'event' : 'product-detail',
    						  'ecommerce' : {
    								'detail' : {
    									'actionField' : {'list' : list},
    									'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    								}
    							}
    					};
    				}
    				break;

    			case "product-addtocart":
    				if (data.hasOwnProperty("commerce")) {
    					var currencyCode = (data.commerce.hasOwnProperty("currency")) ?  data.commerce.currency : "EUR";
    					gtmData = {
    							'event' : 'product-addtocart',
    						  'ecommerce' : {
    								'currencyCode' : currencyCode,
    								'add' : {
    									'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    								}
    							}
    					};
    				}
    				break;

    			case "product-removefromcart":
    				if (data.hasOwnProperty("commerce")) {
    					var currencyCode = (data.commerce.hasOwnProperty("currency")) ?  data.commerce.currency : "EUR";
    					gtmData = {
    							'event' : 'product-removefromcart',
    						  'ecommerce' : {
    								'currencyCode' : currencyCode,
    								'remove' : {
    									'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    								}
    							}
    					};
    				}
    				break;

    			case "promotion-impression":
    				if (data.hasOwnProperty("info")) {
    					// single element
    					gtmData = {
    						  'event' : 'promotion-impression',
    					    'ecommerce' : {
    							'promoView' : {
    								'promotions' : (isArray(data.info)) ? clone(data.info) : [clone(data.info)]
    							}
    						}
    					};
    				}
    				break;

    			case "promotion-click":
    				if (data.hasOwnProperty("info")) {
    					gtmData = {
    							'event' : 'promotion-click',
    							'ecommerce' : {
    								'promoClick' : {
    									'promotions' : (isArray(data.info)) ? clone(data.info) : [clone(data.info)]
    								}
    							}
    					};
    				}
    				break;

    		  case "checkout-step":
    				if (data.hasOwnProperty("commerce")) {
    					gtmData = {
    							'event' : 'checkout',
    							'ecommerce' : {
    								'checkout' : {
    									'actionField' : clone (data.info),
    									'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    								}
    							}
    					};
    				}
    				break;

    			case "checkout-option":
    				var step = "", option = "";
    				if (data.hasOwnProperty("info")) {
    					gtmData = {
    							'event' : 'checkoutOption',
    							'ecommerce' : {
    								'checkout_option' : {
    									'actionField' : clone(data.info)
    								}
    							}
    					};
    				}
    				break;

    			case "checkout-purchase":
    				if (data.hasOwnProperty("commerce")) {
    					gtmData = {
    						  'event' : 'checkout-purchase',
    							'ecommerce' : {
    								'purchase' : {
    									'actionField' : clone(data.info),
    									'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    								}
    							}
    					};
    				}
    				break;

    			case "checkout-refund":
    				if (data.hasOwnProperty("info")) {
    					if (data.hasOwnProperty("commerce")) {
    						gtmData = {
    							  'event' : 'checkout-refund',
    								'ecommerce' : {
    									'refund' : {
    										'actionField' : clone(data.info),
    										'products' : (isArray(data.commerce)) ? clone(data.commerce) : [clone(data.commerce)]
    									}
    								}
    						};
    					} else {
    						gtmData = {
    							  'event' : 'checkout-refund',
    								'ecommerce' : {
    									'refund' : {
    										'actionField' : clone(data.info)
    									}
    								}
    						};
    					}
    				}
    				break;

    			default:
    				for (var attrname in data) {
    				      if (data.hasOwnProperty(attrname)) {
    				        	gtmData[attrname] = data[attrname];
    				      }
    				}
    			  break;
    		}

        if ((typeof w != "undefined") && (w != null)) {
          w.dataLayer.push(gtmData);
        }

    	});
    } catch (e) {
      console.log("GTMEE tracker ERROR: " + e.name + ": " + e.message);
    }
  }

  _instance  = analyticsTransGTMEE();
  return analyticsTransGTMEE.prototype.getInstance;
}));
